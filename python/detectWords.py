# Load the libraries
import sys
import cv2
from pytesseract import pytesseract
from pytesseract import Output

pytesseract.tesseract_cmd = 'C:\\Program Files\\Tesseract-OCR\\tesseract.exe'

#print('Number of arguments:', len(sys.argv), 'arguments.')
#print('Argument List:', str(sys.argv))

pathImage = './public/images/'

nameImage =  sys.argv[1]
#print(" nameImage = "+ nameImage)

typeImage = nameImage[nameImage.index('.'):len(nameImage)]
#print(" extension = "+ typeImage)


nameImageAfterProcess = nameImage[0:-(len(nameImage)-nameImage.index('.'))]+"_processed"+typeImage
#print(" nameImageAfterProcess = "+ nameImageAfterProcess)

img = cv2.imread(pathImage + nameImage)

h, w, c = img.shape
#print('width:  ', w)
#print('height: ', h)
#print('channel:', c)

#percent by which the image is resized
scale_percent = 50

#calculate the 50 percent of original dimensions
width = int(img.shape[1] * scale_percent / 100)
height = int(img.shape[0] * scale_percent / 100)

# dsize
dsize = (width, height)

# resize image
output = cv2.resize(img, dsize)

#save image as uploaded
cv2.imwrite('./public/images/' + nameImage,output)

image_data = pytesseract.image_to_data(img, output_type=Output.DICT)
words= []

for i, word in enumerate(image_data['text']):
    if word != "":
        x,y,w,h = image_data['left'][i],image_data['top'][i],image_data['width'][i],image_data['height'][i]
        cv2.rectangle(img, (x,y), (x+w,y+h), (0,255,0),3)
        cv2.putText(img,word,(x,y-16),cv2.FONT_HERSHEY_COMPLEX,1,(0,0,255),2)
        #print(word)
        #get all words
        words.append("$"+word+"$")


# show all words detected
print(words)

#calculate the 50 percent of original dimensions
width = int(img.shape[1] * scale_percent / 100)
height = int(img.shape[0] * scale_percent / 100)

# dsize
dsize = (width, height)

# resize image
output = cv2.resize(img, dsize)
#save image after _processed

cv2.imwrite('./public/images/' + nameImageAfterProcess,output)


#print(pytesseract.image_to_string(img))
#cv2.imshow('Result',img)
#cv2.waitKey(0)

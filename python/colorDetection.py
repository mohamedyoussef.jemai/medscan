from __future__ import print_function
import binascii
import struct
from PIL import Image
import numpy as np
import scipy
import scipy.misc
import scipy.cluster
import webcolors
import sys
import cv2


NUM_CLUSTERS = 5
pathImage = './public/images/'
nameImage= sys.argv[1]
im = Image.open(pathImage+nameImage)
im = im.resize((150, 150))      # optional, to reduce time
ar = np.asarray(im)
shape = ar.shape
ar = ar.reshape(np.product(shape[:2]), shape[2]).astype(float)

#print('finding clusters')
codes, dist = scipy.cluster.vq.kmeans(ar, NUM_CLUSTERS)
#print('cluster centres:\n', codes)

vecs, dist = scipy.cluster.vq.vq(ar, codes)         # assign codes
counts, bins = np.histogram(vecs, len(codes))    # count occurrences

index_max = np.argmax(counts)                    # find most frequent
peak = codes[index_max]
colour = binascii.hexlify(bytearray(int(c) for c in peak)).decode('ascii')
#print('most frequent is %s (#%s)' % (peak, colour))


def closest_colour(peak):
    min_colours = {}
    for key, name in webcolors.CSS3_HEX_TO_NAMES.items():
        r_c, g_c, b_c = webcolors.hex_to_rgb(key)
        rd = (r_c - peak[0]) ** 2
        gd = (g_c - peak[1]) ** 2
        bd = (b_c - peak[2]) ** 2
        min_colours[(rd + gd + bd)] = name
    return min_colours[min(min_colours.keys())]

def get_colour_name(peak):
    try:
        closest_name = actual_name = webcolors.rgb_to_name(peak)
    except ValueError:
        closest_name = closest_colour(peak)
        actual_name = None
    return actual_name, closest_name

actual_name, closest_name = get_colour_name(peak)
print(str(closest_name))

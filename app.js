// initiate express
const express = require('express');
// initiate mongoose
const mongoose = require('mongoose');
const cors = require('cors');
let bodyParser = require("body-parser");

//The jwtToken verification function
const verify = require("./Utils/verifyToken");


// Include the node file module youssef
var fs = require('fs');
const path = require('path');
multer = require('multer');//

//read python scripts
const {spawn} = require('child_process');


const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(cors());

const port = process.env.PORT || 8000;

app.use(bodyParser.urlencoded({extended: false}));

// parse postman of content-type - application/json
app.use(bodyParser.json());


//models
require("./models/user.model");
require("./models/medicament.model");

//router
let indexRouter = require('./routes/index');
let userRouter = require('./routes/user');
let medicamentRouter = require('./routes/medicament');


//url routes
app.use('/', indexRouter);
app.use('/users', userRouter);
app.use('/medicaments', medicamentRouter);

//passports
require('./config/passport');

app.get('/uploads/:upload', function (req, res) {
    file = req.params.upload;
    console.log(req.params.upload);
    var img = fs.readFileSync(__dirname + '/public/images/' + file);
    res.writeHead(200, {'Content-Type': 'image/png'});
    res.end(img, 'binary');

});

//database connexion
mongoose.connect("mongodb://localhost:27017/medScan", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
})
    .then((data) => console.log("database connected"))
    .catch((err) => console.log(err));

app.listen(port, () => {
    console.log('Server app listening on port ' + port);
});


//upload image and process

storage = multer.diskStorage({
    destination: './public/images/',
    filename: (req, file, cb) => {
        return cb(null, `${file.fieldname}_${Date.now()}${path.extname(file.originalname)}`)
    }
});
//upload image
const upload = multer({
    storage: storage,
    limits: {
        filesize: 1000000
    }
})
let User = mongoose.model('User');
let Medicament = mongoose.model('Medicament');
app.post(
    "/upload", verify, multer({
        storage: storage
    }).single('upload'), function (req, res) {

        console.log(req.file);
        console.log(req.body);


        let BlueColors = ["BLUE", "CADETBLUE", "CORNFLOWERBLUE", "DARKBLUE", "DEEPSKYBLUE", "DODGERBLUE", "LIGHTBLUE", "LIGHTSKYBLUE", "LIGHTSTEELBLUE", "MEDIUMBLUE", "MIDNIGHTBLUE", "POWDERBLUE", "ROYALBLUE", "SKYBLUE", "STEELBLUE", "AQUA", "CYAN", "DARKCYAN", "LIGHTCYAN", "PALETURQUOISE", "AQUAMARINE", "DARKTURQUOISE", "TURQUOISE", "MEDIUMTURQUOISE", "NAVY"];


        let OrangeColors = ["LIGHTSALMON", "CORAL", "TOMATO", "ORANGE", "ORANGERED", "DARKORANGE", "CHOCOLATE"];


        let WhiteColors = ["ANTIQUEWHITE", "WHITE", "SNOW", "HONEYDEW", "MINTCREAM", "AZURE", "ALICEBLUE", "GHOSTWHITE", "WHITESMOKE", "SEASHELL", "BEIGE", "OLDLACE", "FLORALWHITE", "IVORY", "LINEN", "LAVENDERBLUSH", "MISTYROSE"];

        let GreyColors = ["GAINSBORO", "LIGHTGRAY", "SILVER", "DARKGRAY", "GRAY", "DIMGRAY", "LIGHTSLATEGRAY", "SLATEGRAY", "DARKSLATEGRAY", "BLACK"];


        let GreenColors = ["GREENYELLOW", "CHARTREUSE", "LAWNGREEN", "LIME", "LIMEGREEN", "PALEGREEN", "LIGHTGREEN", "MEDIUMSPRINGGREEN", "SPRINGGREEN", "MEDIUMSEAGREEN", "SEAGREEN", "FORESTGREEN", "GREEN", "DARKGREEN", "YELLOWGREEN", "OLIVEDRAB", "OLIVE", "DARKOLIVEGREEN", "MEDIUMAQUAMARINE", "DARKSEAGREEN", "LIGHTSEAGREEN", "TEAL"];

        let PurpleColors = ["LAVENDER", "THISTLE", "PLUM", "VIOLET", "ORCHID", "FUCHSIA", "MAGENTA", "MEDIUMORCHID", "MEDIUMPURPLE", "REBECCAPURPLE", "BLUEVIOLET", "DARKVIOLET", "DARKORCHID", "DARKMAGENTA", "PURPLE", "INDIGO", "SLATEBLUE", "DARKSLATEBLUE", "MEDIUMSLATEBLUE"];

        var id = req.body.id;
        var dataToSend;
        var dataToSend2 = "";
        var prix = "";
        let name = "";
        let color = "None";


        // spawn new child process to call the python script
        const pythonProcess = spawn('python', ["./python/colorDetection.py", req.file.filename]);
// collect data from script


        pythonProcess.stdout.on('data', function (data2) {
            dataToSend2 = data2.toString();
            dataToSend2 = dataToSend2.replace("\r\n", '');
            console.log("dataToSend2 : " + dataToSend2);

            if (BlueColors.includes(dataToSend2.toUpperCase()) || GreenColors.includes(dataToSend2.toUpperCase()) || PurpleColors.includes(dataToSend2.toUpperCase())) {
                color = "Blue";
            }
            if (OrangeColors.includes(dataToSend2.toUpperCase())) {
                color = "Orange";
            }
            if (WhiteColors.includes(dataToSend2.toUpperCase()) || GreyColors.includes(dataToSend2.toUpperCase())) {
                color = "White";
                console.log("***********************************************************************************");
                console.log("***********************************************************************************");
            }

        });


// spawn new child process to call the python script
        const pythonProcess2 = spawn('python', ["./python/detectWords.py", req.file.filename]);
// collect data from script

        pythonProcess2.stdout.on('data', function (data) {

            console.log('Pipe data from python script ...');
            dataToSend = data.toString();

            dataToSend = dataToSend.replace("\r\n", '');
            let onlyLettersArray = dataToSend.split('').filter(char => /[a-zA-Z0-9.$+]/.test(char));
            var finalWords = [];
            var word = "";

            for (let i = 0; i < onlyLettersArray.length; i++) {
                if (onlyLettersArray[i] != "$") {
                    word += onlyLettersArray[i];
                } else if (word != "") {
                    console.log("word " + word);
                    word = word.toUpperCase();
                    finalWords.push(word);
                    word = "";
                }
            }
            //get price
            var indexDT = finalWords.indexOf('DT');
            if (indexDT != -1) {
                prix = finalWords[indexDT - 1] + finalWords[indexDT];
            } else {
                for (let i = 0; i < finalWords.length; i++) {
                    if (finalWords[i].includes('PPT')) {
                        for (let j = i + 1; j < finalWords.length; j++) {

                            indexDT = j + 1;
                            prix += finalWords[j];
                            console.log("prix ==== " + prix);
                        }
                        break;
                    } else if (finalWords[i].includes('+')) {
                        indexDT = i;
                        finalWords[i].replace('PRIX', '');
                        prix = finalWords[indexDT];
                    }
                    if (finalWords[i].includes('PRIX')) {
                        indexDT = i + 1;
                        prix = finalWords[indexDT];
                    }
                }
            }
            if (indexDT == -1) {
                prix = "pas marqué";
            }
            //get name

            let typoWords = ['VIGNETTE', 'PRIX', 'DT', 'EXP', 'LOT', 'MG', 'JAN', 'FEV', 'MAR',
                'AVR', 'MAI', 'JUN', 'JUL', 'AOU', 'SEP', 'OCT', 'NOV', 'DEC', 'AMM', 'BTE', 'COMPRIM',
                'SCABLE', 'BOLTE', 'DE', "COMP", "COMPRIMES", "AV", "AY", "TABLET", "COMPRIME", "GLULES", "GELULES", "GLULE", "PPT", "SA", "TUNISIE",
                "COMPRIMS", "SCABIES", "VISA", "TN", "PP"];

            for (let i = 0; i < finalWords.length; i++) {

                if (!/[^a-zA-Z]/.test(finalWords[i]) && finalWords[i].length > 1 && !typoWords.includes(finalWords[i].toUpperCase())) {
                    name += finalWords[i] + " ";
                }

            }
            //result
        });
// in close event we are sure that stream from child process is closed
        pythonProcess2.on('exit', (code) => {
            console.log(`child process close all stdio with code ${code}`);
            // send data to browser

            const medicament = new Medicament({
                color: color,
                name: name,
                prix: prix,
                image: req.file.filename,
                user: id
            });

            // Save User in the database
            medicament.save(medicament)
                .then(data => {
                    res.send(data);
                })
                .catch(err => {
                    console.log("error: " + err);
                    res.status(401).send({messsage: "Unauthorized action"});

                });

        });

    });


module.exports = app;

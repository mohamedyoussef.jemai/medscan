const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Define collection and schema of User
let Medicament = new Schema({
    name: {
        type: String,
        required: true
    },
    prix: {
        type: String
    },
    color: {
        type: String,
       enum: ['Blue', 'Orange', 'White', 'None'],
        default: "None"
    },
    image: {
        type: String
    },
    user: {type: Schema.Types.ObjectId, ref: 'User'},
}, {

    collection: 'medicaments',
    timestamps: true
})

module.exports = mongoose.model('Medicament', Medicament);


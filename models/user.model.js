const mongoose = require('mongoose');
const Schema = mongoose.Schema;
// crypto for password and token for login
let crypto = require('crypto');
//jwt token
let jwt = require('jsonwebtoken');

// Define collection and schema of User
let User = new Schema({
    username: {
        type: String,
        required: true
    },
    email: {
        type: String
    },
    hash: {
        type: String
    },
    salt: {
        type: String
    },
    images : {
        type : Array , "default" : []
    },
    medicaments : {
        type : Array , "default" : []
    }
}, {
    collection: 'users',
    timestamps: true
})

//set Password crypted
User.methods.setPassword = function (password) {
    this.salt = crypto.randomBytes(16).toString('hex');
    this.hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha512').toString('hex');
};

//validate password crypted
User.methods.validPassword = function (password) {
    var hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha512').toString('hex');
    return this.hash === hash;
};

//generate jwt token
User.methods.generateJwt = function () {
    var expiry = new Date();
    expiry.setDate(expiry.getDate() + 7);

    return jwt.sign({
        _id: this._id,
        email: this.email,
        username: this.username,
        exp: parseInt(expiry.getTime() / 1000),
    }, "MY_SECRET"); // DO NOT KEEP YOUR SECRET IN THE CODE!
};


module.exports = mongoose.model('User', User);


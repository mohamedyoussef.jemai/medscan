let express = require('express');
let router = express.Router();
let userController = require('../controllers/userController');


/* GET All users listing. */
router.post('/add-user', userController.create);
router.get('/', userController.findAll);
router.delete('/', userController.deleteAll);

router.post('/traitementWords', userController.traitementWords);

module.exports = router;

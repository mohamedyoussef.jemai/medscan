let express = require('express');
let router = express.Router();
let medicamentController = require('../controllers/medicamentController');


/* GET All users listing. */
router.get('/:id', medicamentController.findByUserId);
router.get('/getColor/:id/:color', medicamentController.findByUserIdAndColor);
router.delete('/', medicamentController.deleteAll);
router.put('/:id', medicamentController.update);


module.exports = router;

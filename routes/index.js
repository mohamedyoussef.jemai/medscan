let express = require('express');
let router = express.Router();


/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', {title: 'Express'});
});

let jwt = require('express-jwt');

let ctrlAuth = require('../controllers/authentication');
let auth = jwt({
  secret: 'MY_SECRET',
  userProperty: 'payload',
  algorithms: ['RS256']
});

// authentication
router.post('/register', ctrlAuth.register);
router.post('/login', ctrlAuth.login);


module.exports = router;

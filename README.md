# MedScan

M vision

# first step 
    //Init node modules 
    npm i 

    *********Python Setup ******
    - Recommended python version : 3.9.7 
    - download Tesseract OCR 
    - python dependencies : 
        pip install opencv-python
        pip install pytesseract  
        pip install webcolors
        pip install numpy
        pip install scipy
        

#second step

    nodemon

#Third step
    
    - download postman file

#Fourth step

    - add-user or register (as u like)
    - get the id of this user
    - insert the id in form data in upload function and upload file
    - Add the key "auth-token" to header with a valid token after login 

#Fifth step

    - Test it (image claire de vignette)
    - voir résultat avec find all users from postman

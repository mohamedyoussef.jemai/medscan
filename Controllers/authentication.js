let passport = require('passport');
let mongoose = require('mongoose');
let User = mongoose.model('User');
let jwt = require('jsonwebtoken');

//login user
module.exports.login = function (req, res) {

    passport.authenticate('local', function (err, user, info) {
        let token;
        // If Passport throws/catches an error
        if (err) {
            res.status(404).json(err);
            return;
        }
        // If a user is found
        if (user) {
            token = user.generateJwt();
            res.status(200);
            res.json({
                "usersname": user.username,
                "email": user.email,
                "id": user.id,
                "token": token,
            });
            console.log(jwt.decode(token, {complete: true}));

        } else {
            // If user is not found
            res.status(401).json(info);
        }
    })(req, res);

};

// add Root
module.exports.register = function (req, res) {

    let email = req.body.email;
    User.findOne({email: email}, function (err, user) {

        if (user) {
            res.status(401);
            res.send({message:"User already exist !"})
        } else {
            let user = new User();
            user.email = req.body.email;
            user.username = req.body.username;
            user.setPassword(req.body.password);

            user.save(function (err) {
                let token;
                token = user.generateJwt();
                res.status(200);
                res.json({
                    "token": token
                });
            });
        }
    })
};


let mongoose = require('mongoose');
let Medicament = mongoose.model('Medicament');

// Retrieve all medicament by user from the database.
exports.findByUserId = (req, res) => {
    Medicament.find({user: req.params.id})
        .then(data => {
            if (data.length == 0) {
                res.send({message: "aucun médicaments"});
            } else {
                res.send(data);
            }
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving medicaments."
            });
        });
};

// Retrieve all medicament by user and color from the database.
exports.findByUserIdAndColor = (req, res) => {
    Medicament.find({user: req.params.id, color: req.params.color})
        .then(data => {
            if (data.length == 0) {
                res.send({message: "aucun médicaments"});
            } else {
                res.send(data);
            }
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving medicaments."
            });
        });
};

// Delete all medicaments from the database.
exports.deleteAll = (req, res) => {
    Medicament.deleteMany()
        .then(data => {
            res.send({
                message: `${data.deletedCount} Medicaments were deleted successfully!`
            });
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while removing all Medicaments."
            });
        });
};
// Update a match by the id in the request
exports.update = (req, res) => {
    if (!req.body) {
        return res.status(400).send({
            message: "Data to update can not be empty!"
        });
    }
    const id = req.params.id;

    Medicament.findByIdAndUpdate(id, {name:req.body.name}, {useFindAndModify: false})
        .then(data => {
            if (!data) {
                res.status(404).send({
                    message: `Cannot update Medicament with id=${id}. Maybe Medicament was not found!`
                });
            } else res.status(200).send({
                message: `Medicament updated`,
                medicament: data
            });
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Medicament with id=" + id
            });

        });

}

let mongoose = require('mongoose');
let User = mongoose.model('User');

// Create and Save a new User
exports.create = async (req, res) => {

    // Validate request
    if (!req.body.username) {
        res.status(400).send({message: "Content can not be empty!"});
        return;
    }

    const user = new User({
        username: req.body.username,
        email: req.body.email,
    });

    //setPassword
    user.setPassword(req.body.password);

    // Save User in the database
    user.save(user)
        .then(data => {
            res.send(user);
        })
        .catch(err => {
            console.log("error: " + err);
            res.status(401).send({messsage: "Unauthorized action"});

        });
};

// Retrieve all Users from the database.
exports.findAll = (req, res) => {
    User.find()
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving users."
            });
        });
};

// Delete all Users from the database.
exports.deleteAll = (req, res) => {
    User.deleteMany()
        .then(data => {
            res.send({
                message: `${data.deletedCount} Users were deleted successfully!`
            });
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while removing all users."
            });
        });
};

// Delete all Users from the database.
exports.traitementWords = (req, res) => {
    let words = req.body.words;

    words = words.replace("\r\n", '');
    let onlyLettersArray = words.split('').filter(char => /[a-zA-Z0-9.$]/.test(char));
    var finalWords = [];
    var word = "";

    for (let i = 0; i < onlyLettersArray.length; i++) {
        if (onlyLettersArray[i] != "$") {
            word += onlyLettersArray[i];
        } else if (word != "") {
            console.log("word " + word);
            word = word.toUpperCase();
            finalWords.push(word);
            word = "";
        }

    }
    //get price
    var  indexDT = finalWords.indexOf('DT');

    console.log("test index :",indexDT);
    //get name

    let  typoWords = ['VIGNETTE','PRIX','DT','EXP','LOT'];
    let name =""

    for (let i = 0; i < finalWords.length; i++) {

        if (!/[^a-zA-Z]/.test(finalWords[i]) && finalWords[i].length>1  && !typoWords.includes(finalWords[i].toUpperCase())){
          name+=finalWords[i]+" ";
        }

    }

    return res.send({prix: finalWords[indexDT-1]+ finalWords[indexDT],name: name});
};

